package me.pcicenas.formatter.controllers;

import me.pcicenas.formatter.services.CountryBankCodeFormatterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * A test for {@link FormatterController}.
 *
 * @author pcicenas
 * @since 2020-06
 */
@RunWith(MockitoJUnitRunner.class)
public class FormatterControllerTest {

    private static final String COUNTRY_CD = "US";
    private static final String BANK_CD = "12345";

    @Mock
    private CountryBankCodeFormatterService countryBankCodeFormatterService;
    @InjectMocks
    private FormatterController formatterController;

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldFormatCode() {
        // when
        formatterController.formatCode(COUNTRY_CD, BANK_CD);

        // then
        verify(countryBankCodeFormatterService).formatCode(COUNTRY_CD, BANK_CD);
    }
}