package me.pcicenas.formatter.services;

import me.pcicenas.formatter.dto.FormatterResponseDTO;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

/**
 * A test for {@link CountryBankCodeFormatterServiceImp}.
 *
 * @author pcicenas
 * @since 2020-06
 */
public class CountryBankCodeFormatterServiceImpTest {

    private static final String COUNTRY_CD = "US";
    private static final String BANK_CD = "12224555";
    private static final String FORMATTED_COUNTRY_BANK_CD = "US 1(222)4(555)";
    private static final String INVALID_COUNTRY_CD = "A";
    private static final String INVALID_BANK_CD = "3";
    private static final String COUNTRY_CD_VALIDATION_ERROR = "The Country code must consist of 2 symbols"
            + " and must not be empty.";
    private static final String BANK_CD_VALIDATION_ERROR = "The Bank code must contain from 2 to 10 symbols"
            + " and must not be empty.";

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldFormatCode() {
        // given
        CountryBankCodeFormatterServiceImp formatterServiceService = new CountryBankCodeFormatterServiceImp();

        // when
        FormatterResponseDTO formatterResponseDTO = formatterServiceService.formatCode(COUNTRY_CD, BANK_CD);

        // then
        assertThat("There must not be eny errors.", formatterResponseDTO.getErrorMessages().size(),
                equalTo(0));
        assertThat("The code was not formatted properly.", formatterResponseDTO.getValue(),
                equalTo(FORMATTED_COUNTRY_BANK_CD));
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldNotFormatCodeWithWrongCountryCd() {
        // given
        CountryBankCodeFormatterServiceImp formatterServiceService = new CountryBankCodeFormatterServiceImp();

        // when
        FormatterResponseDTO formatterResponseDTO = formatterServiceService.formatCode(INVALID_COUNTRY_CD, BANK_CD);

        // then
        assertThat("The error message is wrong.", formatterResponseDTO.getErrorMessages().get(0),
                equalTo(COUNTRY_CD_VALIDATION_ERROR));
        assertThat("The code must be null.", formatterResponseDTO.getValue(), nullValue());
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldNotFormatCodeWithWrongBankCd() {
        // given
        CountryBankCodeFormatterServiceImp formatterServiceService = new CountryBankCodeFormatterServiceImp();

        // when
        FormatterResponseDTO formatterResponseDTO = formatterServiceService.formatCode(COUNTRY_CD, INVALID_BANK_CD);

        // then
        assertThat("The error message is wrong.", formatterResponseDTO.getErrorMessages().get(0),
                equalTo(BANK_CD_VALIDATION_ERROR));
        assertThat("The code must be null.", formatterResponseDTO.getValue(), nullValue());
    }
}