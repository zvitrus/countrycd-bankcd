package me.pcicenas.formatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@ImportResource("classpath:application-context.xml")
public class CodeFormatterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeFormatterApplication.class, args);
	}

}
