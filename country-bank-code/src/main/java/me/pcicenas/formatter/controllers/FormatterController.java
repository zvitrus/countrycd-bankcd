package me.pcicenas.formatter.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import me.pcicenas.formatter.dto.FormatterResponseDTO;
import me.pcicenas.formatter.services.CountryBankCodeFormatterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * A REST controller implementation responsible for operations related to formatting a Country code and a Bank code.
 *
 * @author pcicenas
 * @since 2020-06
 */
@RestController
@RequestMapping("/formatter")
public class FormatterController {

    @Autowired()
    private CountryBankCodeFormatterService countryBankCodeFormatterService;

    /**
     * Formats a given Country code and a Bank code.
     *
     * @param countryCd a country code to be formatted.
     * @param bankCd    a bank code to be formatted.
     * @return an instance of the {@link FormatterResponseDTO} containing a value formatted out of the country code
     * and the bank code or error messages if formatting does not succeed.
     * @author pcicenas
     * @since 2020-06
     */
    @GetMapping("format")
    @ApiOperation(value = "Converts a Country code and a Bank code", notes = "Returns a formatted value.")
    @ResponseStatus(HttpStatus.OK)
    public FormatterResponseDTO formatCode(@ApiParam(value = "Country code") @Nullable @RequestParam String countryCd,
                                           @ApiParam(value = "Bank code") @Nullable @RequestParam String bankCd) {
        return countryBankCodeFormatterService.formatCode(countryCd, bankCd);
    }

    public CountryBankCodeFormatterService getCountryBankCodeFormatterService() {
        return countryBankCodeFormatterService;
    }

    public void setCountryBankCodeFormatterService(CountryBankCodeFormatterService countryBankCodeFormatterService) {
        this.countryBankCodeFormatterService = countryBankCodeFormatterService;
    }
}
