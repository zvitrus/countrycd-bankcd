package me.pcicenas.formatter.services;

import me.pcicenas.formatter.dto.FormatterResponseDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An implementation of {@link CountryBankCodeFormatterService}.
 *
 * @author pcicenas
 * @since 2020-06
 */
@Service
public class CountryBankCodeFormatterServiceImp implements CountryBankCodeFormatterService {

    private static final String COUNTRY_CD_VALIDATION_ERROR = "The Country code must consist of 2 symbols"
            + " and must not be empty.";
    private static final String BANK_CD_VALIDATION_ERROR = "The Bank code must contain from 2 to 10 symbols"
            + " and must not be empty.";

    /**
     * {@inheritDoc}
     *
     * @author pcicenas
     * @since 2020-06
     */
    @Override
    public FormatterResponseDTO formatCode(String countryCd, String bankCd) {

        FormatterResponseDTO formatterResponseDTO = new FormatterResponseDTO();
        if (!isCountryCdValid(countryCd)) {
            formatterResponseDTO.getErrorMessages().add(COUNTRY_CD_VALIDATION_ERROR);
        }
        if (!isBankCdValid(bankCd)) {
            formatterResponseDTO.getErrorMessages().add(BANK_CD_VALIDATION_ERROR);
        }
        if (formatterResponseDTO.getErrorMessages().isEmpty()) {
            formatterResponseDTO.setValue(countryCd + " " + formatBankCd(bankCd));
        }
        return formatterResponseDTO;
    }

    /**
     * Formats a provided bank code. If there are 2 or more identical symbols following each other in the code,
     * the longest sequence of identical symbols are placed between parentheses. If there are more than one sequence of
     * identical symbols containing the highest number of elements, all the latter groups are placed between parentheses.
     *
     * @param bankCd a code to format.
     * @return A formatted bank code.
     * @author pcicenas
     * @since 2020-06
     */
    private String formatBankCd(String bankCd) {
        List<String> sequences = findSequencesOfIdenticalSymbols(bankCd);
        if (sequences == null || sequences.isEmpty()) {
            return bankCd;
        }
        List<String> longestSequences = retrieveLongestSequences(sequences);
        String formattedBankCd = bankCd;

        if (longestSequences != null && formattedBankCd != null) {
            for (String s : longestSequences) {
                formattedBankCd = formattedBankCd.replace(s, "(" + s + ")");
                formattedBankCd = formattedBankCd.replace("((" + s + "))", "(" + s + ")");
            }
        }

        return formattedBankCd;
    }

    /**
     * Finds sequences of identical symbols in a given String.
     *
     * @param s A string to look for sequences in.
     * @return A list of sequences containing identical symbols.
     * @author pcicenas
     * @since 2020-06
     */
    protected List<String> findSequencesOfIdenticalSymbols(String s) {
        String sequenceOfIdenticalSymbols = "";
        List<String> listOfSequences = new ArrayList<>();
        boolean doesPreviousCharMatch = false;

        for (int i = 0; i < s.length() - 1; i++) {

            if (s.charAt(i) == s.charAt(i + 1)) {
                sequenceOfIdenticalSymbols += s.charAt(i);
                doesPreviousCharMatch = true;
            } else {
                sequenceOfIdenticalSymbols += doesPreviousCharMatch ? s.charAt(i) : "";
                doesPreviousCharMatch = false;

                if (sequenceOfIdenticalSymbols.length() > 0) {
                    listOfSequences.add(sequenceOfIdenticalSymbols);
                }
                sequenceOfIdenticalSymbols = "";
            }
            if (i == s.length() - 2) {
                sequenceOfIdenticalSymbols += doesPreviousCharMatch ? s.charAt(s.length() - 1) : "";
                if (sequenceOfIdenticalSymbols.length() > 0) {
                    listOfSequences.add(sequenceOfIdenticalSymbols);
                }
            }
        }

        return listOfSequences;
    }

    /**
     * Retrieves the longest String contained by a given list. Several Strings are retrieved if there are more
     * than one of the same length.
     *
     * @param sequences A list of Strings.
     * @return A list containing the longest String or several longest Strings. If {@code null} is passed
     * as a parameter to the method, {@code null} is returned.
     * @author pcicenas
     * @since 2020-06
     */
    private List<String> retrieveLongestSequences(List<String> sequences) {
        if (sequences == null) {
            return null;
        }
        List<String> sortedSequences = sequences.stream()
                .sorted((o1, o2) -> o2.length() - o1.length())
                .collect(Collectors.toList());

        List<String> longestSequences = sortedSequences.stream()
                .filter(o -> o.length() >= sortedSequences.get(0).length())
                .collect(Collectors.toList());

        return longestSequences;
    }

    /**
     * Validates a provided bank code that must contain from 2 to 10 symbols.
     *
     * @param bankCd a code to validate.
     * @return {@code true} if the code is valid, {@code false} otherwise.
     * @author pcicenas
     * @since 2020-06
     */
    private boolean isBankCdValid(String bankCd) {
        return bankCd != null && bankCd.length() > 1 && bankCd.length() < 11;
    }

    /**
     * Validates a provided Country code that must contain 2 symbols.
     *
     * @param countryCd a code to validate.
     * @return {@code true} if the code is valid, {@code false} otherwise.
     * @author pcicenas
     * @since 2020-06
     */
    protected boolean isCountryCdValid(String countryCd) {
        return countryCd != null && countryCd.length() == 2;
    }
}
