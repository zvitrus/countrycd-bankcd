package me.pcicenas.formatter.services;

import me.pcicenas.formatter.dto.FormatterResponseDTO;

/**
 * A service responsible for combining a country code and a bank code.
 *
 * @author pcicenas
 * @since 2020-06
 */
public interface CountryBankCodeFormatterService {

    /**
     * Creates a combination of a Country code and a Bank code.
     *
     * @param countryCd a country code to be formatted.
     * @param bankCd    a bank code to be formatted.
     * @return an instance of the {@link FormatterResponseDTO} containing a value formatted out of the country code
     * and the bank code or error messages if formatting does not succeed.
     * @author pcicenas
     * @since 2020-06
     */
    FormatterResponseDTO formatCode(String countryCd, String bankCd);
}
