import React from "react";
import ReactDOM from "react-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import CodeFormatterContainer from "./components/code-formatter/CodeFormatterContainer";

ReactDOM.render(
  <BrowserRouter>
    <App>
      <Switch>
        <Route exact path="/" component={CodeFormatterContainer} />
      </Switch>
    </App>
  </BrowserRouter>,
  document.getElementById("root")
);
