import React, { Component } from "react";
import PropTypes from "prop-types";

const CodeFormatterComponent = props => {
  return (
    <div className="container-fluid">
      <h3>Country/bank code converter</h3>
      <form>
        <div className="">
          <label className="m-2">Šalies kodas</label>
          <input
            className="form-control col-2 m-2"
            placeholder="Įveskite šalies kodą"
            type="text"
            onChange={props.onCountryCode}
          />
          <label className="m-2">Banko kodas</label>
          <input
            className="form-control col-2 m-2"
            placeholder="Įveskite bako kodą"
            type="text"
            onChange={props.onBankCode}
          />
        </div>
        {/* <label className="m-2 ">Amount</label>
        <br />
        <input
          className="m-2 form-control col-2"
          type="text"
          onChange={props.onAmount}
        /> */}
        <label className="m-2 ">Formatted code: </label>
        <span className="italic-style-small">{props.formattedCd}</span>
        <span className="italic-style-small">{props.errorMessages}</span>
        <br />

        <button
          type="button"
          className="btn btn-info m-2"
          onClick={props.onSubmit}
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default CodeFormatterComponent;
