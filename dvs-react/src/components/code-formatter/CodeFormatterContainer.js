import React, { Component } from "react";
import axios from "axios";
import CodeFormatterComponent from "./CodeFormatterComponent";

class CodeFormatterContainer extends Component {
  state = {
    countryCd: "",
    bankCd: "",
    formattedCd: "",
    errorMessages: []
  };

  handleSubmit = e => {
    axios({
      url: "/formatter/format",
      method: "get",
      params: {
        countryCd: this.state.countryCd,
        bankCd: this.state.bankCd
      }
    })
      .then(response => {
        this.setState({ formattedCd: response.data.value });
        this.setState({ errorMessages: response.data.errorMessages });
      })
      .catch(function(error) {
        //it works without catch block as well
        console.log(error);
        if (error.response) {
          //HTTP error happened
          console.log(
            "An error. HTTP error/status code=",
            error.response.status
          );
        } else {
          //some other error happened
          console.log("An error. HTTP error/status code=", error.message);
        }
      });
  };

  handleCountryCd = e => {
    let countryCd = e.target.value;
    this.setState({ countryCd: countryCd });
  };

  handleBankCd = e => {
    let bankCd = e.target.value;
    this.setState({ bankCd: bankCd });
  };

  render() {
    return (
      <CodeFormatterComponent
        onSubmit={this.handleSubmit}
        onCountryCode={this.handleCountryCd}
        onBankCode={this.handleBankCd}
        formattedCd={this.state.formattedCd}
        errorMessages={this.state.errorMessages}
      />
    );
  }
}

export default CodeFormatterContainer;
